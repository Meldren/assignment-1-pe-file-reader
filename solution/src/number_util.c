/// @brief File contains utility methods for numbers
/// @file

#include "number_util.h"

/**
 * @brief Converts a little-endian 32-bit value to big-endian
 * @param[in] val The value to be converted
 * @return The converted value
 */
uint32_t little_to_big_endian(const uint32_t val) {
    return ((val & 0xff000000) >> 24) |
           ((val & 0x00ff0000) >> 8) |
           ((val & 0x0000ff00) << 8) |
           ((val & 0x000000ff) << 24);
}
