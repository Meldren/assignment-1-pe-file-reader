/// @brief Main application file
/// @file

#include "pe_file.h"
#include "number_util.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/// Application name string
#define APP_NAME "section-extractor"

/**
 * @brief Reads the Portable Executable (PE) file and stores the relevant information
 * @param[in] in Pointer to input PE file
 * @param[in,out] file Pointer to PE file data structure where the read data is stored
 * @return 0 in case of success, 1 otherwise
 */
int8_t read_pe_file(FILE *in, struct PEFile *file) {
    if (!in) {
        return 1;
    }
    fseek(in, SIGNATURE_OFFSET, SEEK_SET);
    uint32_t offset;
    if (fread(&offset, sizeof(uint32_t), 1, in) != 1) {
        printf("Failed to read signature offset.\n");
        return 1;
    }
    fseek(in, offset, SEEK_SET);
    if (fread(&file->signature, sizeof(file->signature), 1, in) != 1) {
        printf("Failed to read signature.\n");
        return 1;
    }
    if (little_to_big_endian(file->signature) != PE_FILE_SIGNATURE) {
        printf("Not a valid PE file.\n");
        return 1;
    }
    if (fread(&file->header, sizeof(file->header), 1, in) != 1) {
        printf("Failed to read PE header.\n");
        return 1;
    }
    file->section_headers = malloc(file->header.number_of_sections * sizeof(struct SectionHeader));
    if (!file->section_headers) {
        printf("Failed to allocate memory for section headers.\n");
        return 1;
    }
    fseek(in, file->header.size_of_optional_header, SEEK_CUR);
    if (fread(file->section_headers, sizeof(struct SectionHeader), file->header.number_of_sections, in)
        != file->header.number_of_sections) {
        printf("Failed to read section headers.\n");
        free(file->section_headers);
        return 1;
    }
    return 0;
}

/**
 * @brief Searches for a section in the PE file with a given name
 * @param[in] file Pointer to PE file data structure
 * @param[in] name Name of the section to be found
 * @return The section header data structure or an empty section header
 */
struct SectionHeader find_section_by_name(const struct PEFile *file, const char *name) {
    for (int16_t i = 0; i < file->header.number_of_sections; i++) {
        if (memcmp(file->section_headers[i].name, name, strlen(name)) != 0) {
            continue;
        }
        return file->section_headers[i];
    }
    return (struct SectionHeader) {0};
}

/**
 * @brief Writes the binary data of a section to an output file
 * @param[in] in Pointer to input PE file
 * @param[in] out Pointer to output binary file
 * @param[in] section_header Section header data structure for the section to be extracted
 */
void write_section_data(FILE *in, FILE *out, const struct SectionHeader section_header) {
    if (section_header.pointer_to_raw_data == 0) {
        return;
    }
    fseek(in, section_header.pointer_to_raw_data, SEEK_SET);
    char *section = malloc(section_header.size_of_raw_data);
    if (!section) {
        printf("Failed to allocate memory for section data.\n");
        return;
    }
    if (fread(section, section_header.size_of_raw_data, 1, in) != 1) {
        printf("Failed to read section data.\n");
        free(section);
        return;
    }
    if (fwrite(section, section_header.size_of_raw_data, 1, out) != 1) {
        printf("Failed to write section data.\n");
    }
    free(section);
}

/**
 * @brief Application entry point
 * @param[in] argc Number of command line arguments
 * @param[in] argv Array of command line argument strings
 * @return 0 in case of success, error code otherwise
 */
int main(int argc, char **argv) {
    if (argc != 4) {
        printf("Usage: " APP_NAME " <source-pe-file> <section-name> <output-bin-image>\n");
        return 1;
    }
    struct PEFile *file = malloc(sizeof(struct PEFile));
    if (!file) {
        printf("Failed to allocate memory for PE file.\n");
        return 1;
    }
    FILE *in = fopen(argv[1], "rb");
    if (!in) {
        printf("Failed to open PE file.\n");
        free(file);
        return 1;
    }
    int8_t code = read_pe_file(in, file);
    if (code != 0) {
        free(file);
        fclose(in);
        return 1;
    }
    char *section_name = argv[2];
    struct SectionHeader section_header = find_section_by_name(file, section_name);
    if (section_header.pointer_to_raw_data == 0) {
        printf("Failed to find section with given name.\n");
        free(file->section_headers);
        free(file);
        fclose(in);
        return 1;
    }
    FILE *out = fopen(argv[3], "wb");
    if (!out) {
        printf("Failed to open output binary file.\n");
        free(file->section_headers);
        free(file);
        fclose(in);
        return 1;
    }
    write_section_data(in, out, section_header);
    free(file->section_headers);
    free(file);
    fclose(in);
    fclose(out);
    return 0;
}
