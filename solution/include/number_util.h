/// @brief File contains utility methods for numbers
/// @file

#ifndef SECTION_EXTRACTOR_NUMBER_UTIL_H
#define SECTION_EXTRACTOR_NUMBER_UTIL_H

#include <stdint.h>

/**
 * @brief Converts a little-endian 32-bit value to big-endian
 * @param[in] val The value to be converted
 * @return The converted value
 */
uint32_t little_to_big_endian(uint32_t val);

#endif
