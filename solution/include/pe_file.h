/// @brief File contains structures for PE file
/// @file

#ifndef SECTION_EXTRACTOR_PE_FILE_C
#define SECTION_EXTRACTOR_PE_FILE_C

#include <stdint.h>

/// Offset to the signature of PE file
#define SIGNATURE_OFFSET 0x3c

/// Signature of PE file
#define PE_FILE_SIGNATURE 0x50450000

/// Max section name length
#define MAX_SECTION_NAME_LEN 8

#ifdef _MSC_VER
    #pragma pack(push, 1)
#endif
/**
 * @brief Structure containing the PE header data
 * The PE header contains information about the executable file,
 * such as the machine type, number of sections,
 * creation time stamp, symbol table information, and file attributes
 * @note This struct is packed to ensure the correct alignment and size of its members
 */
struct
#if defined __clang__ || defined __GNUC__
    __attribute__((packed))
#endif
PEHeader {

    /// The number that identifies the type of target machine
    int16_t machine;

    /// The number of sections. This indicates the size of the section table, which immediately follows the headers
    int16_t number_of_sections;

    /// The low 32 bits of the number of seconds since 00:00 January 1, 1970, which indicates when the file was created
    int32_t time_date_stamp;

    /// The file offset of the COFF symbol table, or zero if no COFF symbol table is present
    int32_t pointer_to_symbol_table;

    /// The number of entries in the symbol table
    int32_t number_of_symbols;

    /// The size of the optional header, which is required for executable files but not for object files
    int16_t size_of_optional_header;

    /// The flags that indicate the attributes of the file
    int16_t characteristics;

};

/**
 * @brief The SectionHeader struct represents a section header in a PE file
 * The section header contains information about a specific section in the PE file,
 * such as its name, virtual and physical address, size, etc
 * @note This struct is packed to ensure the correct alignment and size of its members
 */
struct
#if defined __clang__ || defined __GNUC__
    __attribute__((packed))
#endif
SectionHeader {

    /// The name of the section
    uint8_t name[MAX_SECTION_NAME_LEN];

    /// The total size of the section when loaded into memory
    union {
        /// The physical address of the section
        uint32_t physical_address;

        /// The virtual size of the section
        uint32_t virtual_size;
    } misc;

    /// The virtual address of the section
    uint32_t virtual_address;

    /// The size of the section in the raw data file
    uint32_t size_of_raw_data;

    /// The file pointer to the section's raw data
    uint32_t pointer_to_raw_data;

    /// The file pointer to the section's relocation entries
    uint32_t pointer_to_relocations;

    /// The file pointer to the section's line-number entries
    uint32_t pointer_to_linenumbers;

    /// The number of relocation entries for the section
    uint16_t number_of_relocations;

    /// The number of line-number entries for the section
    uint16_t number_of_linenumbers;

    /// The characteristics of the section, such as permissions and flags
    uint32_t characteristics;

};

#ifdef _MSC_VER
    #pragma pack(pop)
#endif

/**
 * @brief Structure containing PE file data
 * The PE file is used to represent the entire PE file,
 * with a signature identifying it as a PE format image file,
 * the main header, and an array of section headers
 */
struct PEFile {

    /// Magic number that identifies the file as a PE format image file
    uint32_t signature;

    /// Main header
    struct PEHeader header;

    /// Array of section headers with the size of header.number_of_sections
    struct SectionHeader *section_headers;

};

#endif
